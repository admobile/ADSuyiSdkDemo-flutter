import 'dart:io';

class KeyManager {
  static String appKey() {
    // appid
    if (Platform.isIOS) {
      return "3437764";
    } else {
      return "3801556";
    }
  }

  static String bannerPosid() {
    // 横幅广告位id
    if (Platform.isIOS) {
      return "9ca1e179e38ca5a35c";
    } else {
      return "467b6588a2a28bff5b";
    }
  }

  static String fullScreenPosid() {
    // 全屏视频广告位id
    if (Platform.isIOS) {
      return "f3953777bc833957d8";
    } else {
      return "6620c9299df9013cf5";
    }
  }

  static String interPosid() {
    // 插屏广告位id
    if (Platform.isIOS) {
      return "9535af29514e548fe0";
    } else {
      return "7b4e3d4697e1c00908";
    }
  }

  static String nativePosid() {
    // 信息流广告位id
    if (Platform.isIOS) {
      return "177a790a315eeb7053";
    } else {
      return "fd3b78d0c93e75330a";
    }
  }

  static String splashPosid() {
    // 开屏广告位id
    if (Platform.isIOS) {
      return "73128265daffdd6a1d";
    } else {
      return "63a981ded04aa89442";
    }
  }

  static String rewardPosid() {
    // 激励视频广告位id
    if (Platform.isIOS) {
      return "47d196ffaaa92ae93c";
    } else {
      return "9ec7704ebcad7364ce";
    }
  }
}
