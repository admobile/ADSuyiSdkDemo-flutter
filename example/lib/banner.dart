import 'package:ad_suyi_flutter_sdk/ad_suyi_ad.dart';
import 'package:ad_suyi_flutter_sdk/ad_suyi_base.dart';
import 'package:flutter/material.dart';

import 'key.dart';

class BannerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _BannerState();
}

class _BannerState extends State<BannerPage> {
  ADSuyiFlutterBannerAd? _adSuyiFlutterBannerAd;
  bool _hasInitBanner = false;

  @override
  Widget build(BuildContext context) {
    if (_adSuyiFlutterBannerAd == null && _hasInitBanner == false) {
      MediaQueryData queryData = MediaQuery.of(context);
      _hasInitBanner = true;
      var width = queryData.size.width;
      var height = queryData.size.width / 320.0 * 50.0;
      _adSuyiFlutterBannerAd = ADSuyiFlutterBannerAd(
          posId: KeyManager.bannerPosid(), width: width, height: height);
      _adSuyiFlutterBannerAd!.loadAndShow();
      // 横幅加载成功回调
      _adSuyiFlutterBannerAd!.onSucced = (AdInfo adInfo) {
        print("横幅广告加载成功 platformName : " +
            adInfo.platformName +
            " ecpmPrecision : " +
            adInfo.ecpmPrecision +
            " ecpm : " +
            adInfo.ecpm.toString());
      };
      // 横幅加载失败回调
      _adSuyiFlutterBannerAd!.onFailed = (AdErrorInfo adErrorInfo) {
        removeBannerAd();
        print("横幅广告加载失败 errorCode : " +
            adErrorInfo.errorCode.toString() +
            " errorDescription : " +
            adErrorInfo.errorDescription);
      };
      // 横幅点击回调
      _adSuyiFlutterBannerAd!.onClicked = (AdInfo adInfo) {
        print("横幅广告点击 platformName : " +
            adInfo.platformName +
            " ecpmPrecision : " +
            adInfo.ecpmPrecision +
            " ecpm : " +
            adInfo.ecpm.toString());
      };
      // 横幅曝光回调
      _adSuyiFlutterBannerAd!.onExposed = (AdInfo adInfo) {
        print("横幅广告渲染成功 platformName : " +
            adInfo.platformName +
            " ecpmPrecision : " +
            adInfo.ecpmPrecision +
            " ecpm : " +
            adInfo.ecpm.toString());
      };
      // 横幅关闭回调
      _adSuyiFlutterBannerAd!.onClosed = (AdInfo adInfo) {
        removeBannerAd();
        print("横幅广告关闭成功 platformName : " +
            adInfo.platformName +
            " ecpmPrecision : " +
            adInfo.ecpmPrecision +
            " ecpm : " +
            adInfo.ecpm.toString());
      };
    }

    return Scaffold(
        appBar: AppBar(
          title: Text("BannerPage"),
        ),
        body: Center(
            child: (_adSuyiFlutterBannerAd == null
                ? Text("banner广告已关闭")
                : ADSuyiWidget(adView: _adSuyiFlutterBannerAd!))));
  }

  void removeBannerAd() {
    setState(() {
      releaseBannerAd();
    });
  }

  void releaseBannerAd() {
    _adSuyiFlutterBannerAd?.release();
    _adSuyiFlutterBannerAd = null;
  }

  @override
  void dispose() {
    releaseBannerAd();
    super.dispose();
  }
}
