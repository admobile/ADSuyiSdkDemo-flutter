import 'package:ad_suyi_flutter_sdk/ad_suyi_ad.dart';
import 'package:ad_suyi_flutter_sdk/ad_suyi_base.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';

import 'main.dart';
import 'key.dart';

class SplashLoadShowSeparatePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashLoadShowSeparateState();
}

class _SplashLoadShowSeparateState extends State<SplashLoadShowSeparatePage> {
  ADSuyiSplashAdLoadShowSeparate? _adSuyiFlutterSplashAd;
  bool _hasInitBanner = false;

  @override
  Widget build(BuildContext context) {
    if (_adSuyiFlutterSplashAd == null && _hasInitBanner == false) {
      MediaQueryData queryData = MediaQuery.of(context);
      _hasInitBanner = true;
      var width = queryData.size.width;
      var height = queryData.size.height;
      _adSuyiFlutterSplashAd = ADSuyiSplashAdLoadShowSeparate(
        posId: KeyManager.splashPosid(),
        width: width, //屏幕宽度
        height: height, //屏幕高度
        imgName: "splash_placeholder", //背景名称
        imgLogoName: "splash_logo", //底部logo名称
      );
      // imgName、imgLogoName iOS 使用
      // 加载开屏广告
      _adSuyiFlutterSplashAd!.load();
      // 开屏广告加载成功回调
      _adSuyiFlutterSplashAd!.onSucced = (AdInfo adInfo) {
        print("开屏广告加载成功 platformName : " +
            adInfo.platformName +
            " ecpmPrecision : " +
            adInfo.ecpmPrecision +
            " ecpm : " +
            adInfo.ecpm.toString());
        // 展示开屏广告（可根据实际情况在合适的时机展示开屏）
        _adSuyiFlutterSplashAd!.show();
      };
      // 开屏广告加载失败回调
      _adSuyiFlutterSplashAd!.onFailed = (AdErrorInfo adErrorInfo) {
        releaseSplashAd();
        print("开屏广告加载失败 errorCode : " +
            adErrorInfo.errorCode.toString() +
            " errorDescription : " +
            adErrorInfo.errorDescription);
        toHome();
      };
      // 开屏广告点击回调
      _adSuyiFlutterSplashAd!.onClicked = (AdInfo adInfo) {
        print("开屏广告点击 platformName : " +
            adInfo.platformName +
            " ecpmPrecision : " +
            adInfo.ecpmPrecision +
            " ecpm : " +
            adInfo.ecpm.toString());
      };
      // 开屏广告曝光回调
      _adSuyiFlutterSplashAd!.onExposed = (AdInfo adInfo) {
        print("开屏广告渲染成功 platformName : " +
            adInfo.platformName +
            " ecpmPrecision : " +
            adInfo.ecpmPrecision +
            " ecpm : " +
            adInfo.ecpm.toString());
      };
      // 开屏广告关闭回调
      _adSuyiFlutterSplashAd!.onClosed = (AdInfo adInfo) {
        releaseSplashAd();
        print("开屏广告关闭成功 platformName : " +
            adInfo.platformName +
            " ecpmPrecision : " +
            adInfo.ecpmPrecision +
            " ecpm : " +
            adInfo.ecpm.toString());
        toHome();
      };
    }

    return new Scaffold(
        body: Container(
            decoration: new BoxDecoration(color: Color(0xff000000)),
            child: (_adSuyiFlutterSplashAd == null
                ? Text("开屏广告已关闭")
                : ADSuyiWidget(adView: _adSuyiFlutterSplashAd!))));
  }

  void toHome() {
    Navigator.pushAndRemoveUntil(
      context,
      new MaterialPageRoute(builder: (context) => new MyApp()),
      (route) => route == null,
    );
  }

  void removeSplashAd() {
    setState(() {
      releaseSplashAd();
    });
  }

  void releaseSplashAd() {
    _adSuyiFlutterSplashAd?.release();
    _adSuyiFlutterSplashAd = null;
  }

  @override
  void dispose() {
    releaseSplashAd();
    super.dispose();
  }
}
