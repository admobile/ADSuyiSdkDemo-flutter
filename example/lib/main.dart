import 'package:ad_suyi_flutter_sdk_example/reward.dart';
import 'package:ad_suyi_flutter_sdk_example/splashLoadShowSeparate.dart';
import 'package:flutter/material.dart';

import 'package:ad_suyi_flutter_sdk_example/banner.dart';
import 'package:ad_suyi_flutter_sdk_example/splash.dart';
import 'package:ad_suyi_flutter_sdk/ad_suyi_flutter_sdk.dart';

import 'fullscreen.dart';
import 'inter.dart';
import 'key.dart';
import 'native.dart';
import 'splash.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();

    AdSuyiFlutterSdk.initSdk(appid: KeyManager.appKey());
  }

  static const listData = [
    "SplashAd(开屏广告)",
    "SplashAd(开屏广告加载展示分离)",
    "Banner(横幅广告)",
    "Native(信息流广告)",
    "Inter(插屏广告)",
    "Reward(激励视频广告)",
    "fullscreenVod(全屏视频广告)"
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: const Text('ADSuyiSDK 广告demo'),
          ),
          body: ListView.builder(
            itemCount: listData.length,
            padding: const EdgeInsets.all(15),
            itemBuilder: (BuildContext context, int index) {
              return Container(
                  margin: EdgeInsets.all(15),
                  decoration: new BoxDecoration(
                    border: new Border.all(
                      width: 1,
                      color: Colors.blue,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                  ),
                  child: GestureDetector(
                    onTap: () {
                      var widget;
                      switch (index) {
                        case 0:
                          // 加载并展示开屏示例
                          widget = SplashPage();
                          break;
                        case 1:
                          // 加载、展示开屏示例
                          widget = SplashLoadShowSeparatePage();
                          break;
                        case 2:
                          // 横幅示例
                          widget = BannerPage();
                          break;
                        case 3:
                          // 信息流示例（模板、自渲染）
                          widget = NativePage();
                          break;
                        case 4:
                          // 插屏示例
                          widget = InterPage();
                          break;
                        case 5:
                          // 激励视频示例
                          widget = RewardPage();
                          break;
                        case 6:
                          // 全屏视频示例
                          widget = FullScreenPage();
                          break;
                      }
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return widget;
                      }));
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(listData[index],
                            style: TextStyle(color: Colors.blue, fontSize: 18),
                            textAlign: TextAlign.center)
                      ],
                    ),
                  ));
            },
          )),
    );
  }
}
