import 'package:flutter/material.dart';
import 'package:ad_suyi_flutter_sdk/ad_suyi_ad.dart';

import 'key.dart';

class FullScreenPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => FullScreenState();
}

class FullScreenState extends State<FullScreenPage> {
  ADSuyiFullScreenVodAd? _fullScreenVodAd;

  @override
  Widget build(BuildContext context) {
    showFullScreenVodAd();
    return Scaffold(
      appBar: AppBar(
        title: Text("FullScreenVodAd"),
      ),
      body: Center(),
    );
  }

  // 全屏视频
  // 显示全屏视频广告请保证当时app内没有其他地方显示全屏视频广告，否则会有冲突
  void showFullScreenVodAd() {
    if (_fullScreenVodAd != null) {
      return;
    }
    _fullScreenVodAd =
        ADSuyiFullScreenVodAd(posId: KeyManager.fullScreenPosid());
    // 全屏视频加载失败回调
    _fullScreenVodAd!.onFailed = (AdErrorInfo adErrorInfo) {
      print("全屏视频广告加载失败 errorCode : " +
          adErrorInfo.errorCode.toString() +
          " errorDescription : " +
          adErrorInfo.errorDescription);
      releaseFullScreenVodAd();
      releaseFullScreenVodAd();
    };
    // 全屏视频曝光回调
    _fullScreenVodAd!.onExposed = (AdInfo adInfo) {
      print("全屏视频广告曝光了 platformName : " +
          adInfo.platformName +
          " ecpmPrecision : " +
          adInfo.ecpmPrecision +
          " ecpm : " +
          adInfo.ecpm.toString());
    };
    // 全屏视频加载成功回调
    _fullScreenVodAd!.onSucced = (AdInfo adInfo) {
      print("全屏视频广告成功了 platformName : " +
          adInfo.platformName +
          " ecpmPrecision : " +
          adInfo.ecpmPrecision +
          " ecpm : " +
          adInfo.ecpm.toString());
      playFullScreenVodAd();
    };
    // 全屏视频点击回调
    _fullScreenVodAd!.onClicked = (AdInfo adInfo) {
      print("全屏视频广告点击了 platformName : " +
          adInfo.platformName +
          " ecpmPrecision : " +
          adInfo.ecpmPrecision +
          " ecpm : " +
          adInfo.ecpm.toString());
    };
    // 全屏视频关闭回调
    _fullScreenVodAd!.onClosed = (AdInfo adInfo) {
      print("全屏视频广告关闭 platformName : " +
          adInfo.platformName +
          " ecpmPrecision : " +
          adInfo.ecpmPrecision +
          " ecpm : " +
          adInfo.ecpm.toString());
      releaseFullScreenVodAd();
    };
    _fullScreenVodAd!.load();
  }

  void releaseFullScreenVodAd() {
    _fullScreenVodAd?.release();
    _fullScreenVodAd = null;
  }

  void playFullScreenVodAd() {
    _fullScreenVodAd!.show();
  }

  @override
  void dispose() {
    releaseFullScreenVodAd();
    super.dispose();
  }
}
