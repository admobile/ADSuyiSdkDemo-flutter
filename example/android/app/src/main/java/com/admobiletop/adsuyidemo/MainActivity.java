package com.admobiletop.adsuyidemo;

import android.util.Log;

import com.sangshen.ad_suyi_flutter_sdk.ADSuyiMobileAdsPlugin;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;

public class MainActivity extends FlutterActivity {
    @Override
    public void configureFlutterEngine(FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        Log.d("configureFlutterEngine", "init");
        flutterEngine.getPlugins().add(new ADSuyiMobileAdsPlugin());
        ADSuyiMobileAdsPlugin.registerNativeAdFactory(flutterEngine, "adFactoryExample");
    }

    @Override
    public void cleanUpFlutterEngine(FlutterEngine flutterEngine) {

    }
}
