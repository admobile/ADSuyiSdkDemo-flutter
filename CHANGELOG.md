## 1.2.2
android:
    *初始化代码优化*

## 1.2.1
android:
    *去除适配器中的kotlin_version版本避免冲突*

## 1.2.0
android:
    *onFailed回调错误信息*
iOS:
    *onFailed回调错误信息*

## 1.1.9
iOS:
    *横幅请求方法变更，需传controller

## 1.1.8
android:
    *onSucced、onClosed、onClicked、onExposed、onRewarded回调增加adInfo回传。
iOS:
    *onSucced、onClosed、onClicked、onExposed、onRewarded回调增加adInfo回传。

## 1.1.7
iOS:
    *针对Flutter适配优化。

## 1.1.6
android:
    *针对Flutter适配优化。

## 1.1.5
android:
    * 安卓初始化增加是否允许读取安装列表、是否允许读写外部权限配置。

## 1.1.4
android:
    * 安卓信息流高度设置优化。

## 1.1.3
android:
    * 解决安卓端信息流广告对象无法重复加载广告端问题。

## 1.1.2
android:
    * 去除获取广告Toast提示。

## 1.1.1
android:
    * 去除new StandardMethodCodec(new AdMessageCodec())避免bugly错误日志上报。

## 1.0.9
ios:
    * 开屏加载并展示功能支持自定义底部logo图片。

## 1.0.8
android:
    * 开屏动态申请权限可取消。

## 1.0.7
android:
    * 开屏适配新增回调。

## 1.0.6
android:
    * 增加开屏展示分离，暂时不支持快手渠道。
ios:
    * 增加开屏展示分离

## 1.0.5
android:
    * 信息流广告展示大小调整，优量汇、百度、快手渠道固定为10：9比例，需要创建广告样式时，尽可能接近该比例，可达最佳展示效果。

## 1.0.4
android:
    * 天目渠道升级

## 1.0.3
android:
    * flutter插件初始化增加合规开关，开屏增加是否允许用户拒绝权限申请后重复请求权限

## 1.0.2

* flutter插件增加隐私开关

## 1.0.1

* android端开屏增加底部logo和背景图.

## 1.0.0

* 修改android端传递参数.

## 0.0.7

* android修改三方仓库地址为https.

## 0.0.6

* 修改一些回调.

## 0.0.1

* TODO: Describe initial release.
