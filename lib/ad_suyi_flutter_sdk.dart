import 'dart:io';
import 'dart:async';
import 'package:ad_suyi_flutter_sdk/ad_suyi_ad.dart';
import 'package:ad_suyi_flutter_sdk/ad_suyi_base.dart';
import 'package:flutter/services.dart';

class AdSuyiFlutterSdk {
  static const MethodChannel _channel = const MethodChannel('ad_suyi_sdk');

  static void _onAdEvent(
      ADSuyiAd ad, String eventName, Map<dynamic, dynamic> arguments) {
    switch (eventName) {
      case 'onSucced':
        if (ad.onSucced != null) {
          final String platformName = arguments['platformName'];
          final String ecpmPrecision = arguments['ecpmPrecision'];
          final double ecpm = arguments['ecpm'];
          AdInfo adInfo = AdInfo(platformName, ecpmPrecision, ecpm);
          ad.onSucced!(adInfo);
        }
        break;
      case 'onFailed':
        if (ad.onFailed != null) {
          final int errorCode = arguments['errorCode'];
          final String errorDescription = arguments['errorDescription'];
          AdErrorInfo adErrorInfo = AdErrorInfo(errorCode, errorDescription);
          ad.onFailed!(adErrorInfo);
        }
        break;
      case 'onExposed':
        if (ad.onExposed != null) {
          final String platformName = arguments['platformName'];
          final String ecpmPrecision = arguments['ecpmPrecision'];
          final double ecpm = arguments['ecpm'];
          AdInfo adInfo = AdInfo(platformName, ecpmPrecision, ecpm);
          ad.onExposed!(adInfo);
        }
        break;
      case 'onClicked':
        if (ad.onClicked != null) {
          final String platformName = arguments['platformName'];
          final String ecpmPrecision = arguments['ecpmPrecision'];
          final double ecpm = arguments['ecpm'];
          AdInfo adInfo = AdInfo(platformName, ecpmPrecision, ecpm);
          ad.onClicked!(adInfo);
        }
        break;
      case 'onClosed':
        if (ad.onClosed != null) {
          final String platformName = arguments['platformName'];
          final String ecpmPrecision = arguments['ecpmPrecision'];
          final double ecpm = arguments['ecpm'];
          AdInfo adInfo = AdInfo(platformName, ecpmPrecision, ecpm);
          ad.onClosed!(adInfo);
        }
        break;
      case 'onRewarded':
        if (ad.onRewarded != null) {
          final String platformName = arguments['platformName'];
          final String ecpmPrecision = arguments['ecpmPrecision'];
          final double ecpm = arguments['ecpm'];
          AdInfo adInfo = AdInfo(platformName, ecpmPrecision, ecpm);
          ad.onRewarded!(adInfo);
        }
        break;
      case 'onReceived':
        final int adViewId = arguments['adViewId'];
        final double width = arguments['adWidth'];
        final double height = arguments['adHeight'];
        ADSuyiFlutterNativeAd nativeAd = ad as ADSuyiFlutterNativeAd;
        nativeAd.onReceivedHandle(adViewId, width, height);
        break;
    }
  }

  static void setupMethodCallHandler() {
    _channel.setMethodCallHandler((MethodCall call) async {
      final int adId = call.arguments['adId'];
      ADSuyiAd ad = instanceManager.adFor(adId);
      _onAdEvent(ad, call.method, call.arguments);
    });
  }

  // 初始化sdk
  static Future<void> initSdk({required String appid}) async {
    setupMethodCallHandler();
    final void result;
    if (Platform.isIOS) {
      result = await _channel.invokeMethod('initSdk', appid);
    } else {
      result = await _channel.invokeMethod('initSdk', {
        "appid": appid,
        "isDebug": true,
        "agreePrivacyStrategy": true, // 是否同意隐私政策
        "isCanUseLocation": true, // 是否允许使用地理位置
        "isCanUsePhoneState": true, // 是否允许使用设备信息
        "isCanUseWifiState": true, // 是否允许使用WIFI状态
        "isCanReadInstallList": true, // 是否允许读取安装列表
        "isCanUseReadWriteExternal": true // 是否允许读写外部权限
      });
    }
    return result;
  }

  // 初始化sdk并设置隐私
  static Future<void> initSdkAndSetPrivacy(
      {required String appid,
      required bool isDebug,
      required bool agreePrivacyStrategy,
      required bool isCanUseLocation,
      required bool isCanUsePhoneState,
      required bool isCanUseWifiState,
      required bool isCanReadInstallList,
      required bool isCanUseReadWriteExternal}) async {
    setupMethodCallHandler();
    final void result;
    if (Platform.isIOS) {
      result = await _channel.invokeMethod('initSdk', appid);
    } else {
      result = await _channel.invokeMethod('initSdk', {
        "appid": appid,
        "isDebug": isDebug,
        "agreePrivacyStrategy": agreePrivacyStrategy,
        "isCanUseLocation": isCanUseLocation,
        "isCanUsePhoneState": isCanUsePhoneState,
        "isCanUseWifiState": isCanUseWifiState,
        "isCanReadInstallList": isCanReadInstallList,
        "isCanUseReadWriteExternal": isCanUseReadWriteExternal
      });
    }
    return result;
  }

  // 设置是否允许展示个性化广告
  static Future<void> setPersonalizedEnabled(
      {required bool personalized}) async {
    setupMethodCallHandler();
    final void result = await _channel
        .invokeMethod('setPersonalizedEnabled', {"personalized": personalized});
    return result;
  }

  static Future<void> releaseAd({required int adId}) async {
    final void result = await _channel.invokeMethod('releaseAd', adId);
    return result;
  }

  // 加载并展示开屏广告
  static Future<void> loadSplashAd({
    required String posId, //广告位id
    required int adId, //广告id
    required String imgName, //开屏过度图片名称
    required String imgLogoName, //开屏底部logo图片名称
    required bool isRepeatApplyPermission, //是否重复申请权限
    required bool isApplyPermission, //是否申请权限
  }) async {
    final void result = await _channel.invokeMethod('loadSplashAd', {
      "posId": posId,
      "adId": adId,
      "imgName": imgName,
      "imgLogoName": imgLogoName,
      "isRepeatApplyPermission": isRepeatApplyPermission,
      "isApplyPermission": isApplyPermission
    });
    return result;
  }

  // 加载开屏
  static Future<void> loadOnlySplashAd({
    required String posId,
    required int adId,
    required double adWidth,
    required double adHeight,
    String? imgName,
    String? imgLogoName,
  }) async {
    final void result = await _channel.invokeMethod('loadOnlySplashAd', {
      "adId": adId,
      "adWidth": adWidth,
      "adHeight": adHeight,
      "posId": posId,
      "imgName": imgName,
      "imgLogoName": imgLogoName,
    });
    return result;
  }

  // 展示开屏广告
  static Future<void> showSplashAd({
    required int adId,
  }) async {
    final void result = await _channel.invokeMethod('showSplashAd', {
      "adId": adId,
    });
    return result;
  }

  // 加载激励视频广告
  static Future<void> loadRewardAd({
    required String posId,
    required int adId,
  }) async {
    final void result = await _channel.invokeMethod('loadRewardAd', {
      "posId": posId,
      "adId": adId,
    });
    return result;
  }

  // 展示激励视频广告
  static Future<void> showRewardAd({
    required int adId,
  }) async {
    final void result = await _channel.invokeMethod('showRewardAd', {
      "adId": adId,
    });
    return result;
  }

  // 加载插屏广告
  static Future<void> loadIntertitialAd({
    required String posId,
    required int adId,
  }) async {
    final void result = await _channel.invokeMethod('loadIntertitialAd', {
      "posId": posId,
      "adId": adId,
    });
    return result;
  }

  // 展示插屏广告
  static Future<void> showIntertitialAd({
    required int adId,
  }) async {
    final void result = await _channel.invokeMethod('showIntertitialAd', {
      "adId": adId,
    });
    return result;
  }

  // 加载全屏视频广告
  static Future<void> loadFullscreenVodAd({
    required String posId,
    required int adId,
  }) async {
    final void result = await _channel.invokeMethod('loadFullScreenVodAd', {
      "posId": posId,
      "adId": adId,
    });
    return result;
  }

  // 展示全屏视频广告
  static Future<void> showFullscreenVodAd({
    required int adId,
  }) async {
    final void result = await _channel.invokeMethod('showFullScreenVodAd', {
      "adId": adId,
    });
    return result;
  }

  // 加载横幅广告
  static Future<void> loadBannerAd({
    required String posId,
    required int adId,
    required double adWidth,
    required double adHeight,
  }) async {
    final void result = await _channel.invokeMethod('loadBannerAd', {
      "adId": adId,
      "adWidth": adWidth,
      "adHeight": adHeight,
      "posId": posId,
    });
    return result;
  }

  // 加载信息流广告
  static Future<void> loadNativeAd({
    required String posId,
    required double width,
    required double height,
    required int adId,
  }) async {
    final void result = await _channel.invokeMethod('loadNativeAd',
        {"posId": posId, "adWidth": width, "adHeight": height, "adId": adId});
    return result;
  }
}
