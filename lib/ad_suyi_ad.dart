import 'dart:io';

import 'ad_suyi_base.dart';
import 'ad_suyi_flutter_sdk.dart';

class ADSuyiSplashAd extends ADSuyiAdLoader {
  String posId;
  String imgName;
  String imgLogoName;
  bool isRepeatApplyPermission;
  bool isApplyPermission;

  ADSuyiSplashAd(
      {required this.posId,
      required this.imgName,
      required this.imgLogoName,
      required this.isRepeatApplyPermission,
      required this.isApplyPermission}) {
    regist();
  }

  void loadAndShow() {
    AdSuyiFlutterSdk.loadSplashAd(
        posId: this.posId,
        adId: this.adId,
        imgName: this.imgName,
        imgLogoName: this.imgLogoName,
        isRepeatApplyPermission: this.isRepeatApplyPermission,
        isApplyPermission: this.isApplyPermission);
  }

  void release() {
    unregist();
  }
}

class ADSuyiSplashAdLoadShowSeparate extends ADSuyiAdView {
  String posId;
  double width;
  double height;
  String? imgName;
  String? imgLogoName;

  ADSuyiSplashAdLoadShowSeparate(
      {required this.posId,
      required this.width,
      required this.height,
      this.imgName,
      this.imgLogoName}) {
    regist();
  }

  void load() {
    AdSuyiFlutterSdk.loadOnlySplashAd(
        posId: this.posId,
        adId: this.adId,
        adWidth: this.width,
        adHeight: this.height,
        imgName: this.imgName,
        imgLogoName: this.imgLogoName);
  }

  void show() {
    AdSuyiFlutterSdk.showSplashAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADSuyiRewardAd extends ADSuyiAdLoader {
  String posId;

  ADSuyiRewardAd({required this.posId}) {
    regist();
  }

  void load() {
    AdSuyiFlutterSdk.loadRewardAd(posId: this.posId, adId: this.adId);
  }

  void show() {
    AdSuyiFlutterSdk.showRewardAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADSuyiIntertitialAd extends ADSuyiAdLoader {
  String posId;

  ADSuyiIntertitialAd({required this.posId}) {
    regist();
  }

  void load() {
    AdSuyiFlutterSdk.loadIntertitialAd(posId: this.posId, adId: this.adId);
  }

  void show() {
    AdSuyiFlutterSdk.showIntertitialAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADSuyiFullScreenVodAd extends ADSuyiAdLoader {
  String posId;

  ADSuyiFullScreenVodAd({required this.posId}) {
    regist();
  }

  void load() {
    AdSuyiFlutterSdk.loadFullscreenVodAd(posId: this.posId, adId: this.adId);
  }

  void show() {
    AdSuyiFlutterSdk.showFullscreenVodAd(adId: this.adId);
  }

  void release() {
    unregist();
  }
}

class ADSuyiFlutterBannerAd extends ADSuyiAdView {
  String posId;
  double width;
  double height;

  ADSuyiFlutterBannerAd(
      {required this.posId, required this.width, required this.height}) {
    regist();
  }

  void loadAndShow() {
    AdSuyiFlutterSdk.loadBannerAd(
        posId: this.posId,
        adId: this.adId,
        adWidth: this.width,
        adHeight: this.height);
  }

  void release() {
    unregist();
  }
}

class ADSuyiFlutterNativeAd extends ADSuyiAdLoader {
  String posId;
  double width;
  double height;

  ADSuyiFlutterNativeAd(
      {required this.posId, required this.width, required this.height}) {
    regist();
  }

  void load() {
    if (Platform.isAndroid) {
      this.resetAdId();
    }
    AdSuyiFlutterSdk.loadNativeAd(
        posId: this.posId,
        width: this.width,
        height: this.height,
        adId: this.adId);
  }

  void onReceivedHandle(int adViewId, double width, double height) {
    ADSuyiFlutterNativeAdView adView = ADSuyiFlutterNativeAdView(adViewId);
    adView.height = height;
    adView.width = width;
    if (this.onReceived != null) {
      this.onReceived!(adView);
    }
  }

  void release() {
    unregist();
  }
}

class ADSuyiFlutterNativeAdView extends ADSuyiAdView {
  ADSuyiFlutterNativeAdView(int adId) {
    this.adId = adId;
    regist();
  }

  void release() {
    unregist();
  }
}

class AdInfo {
  String platformName;
  String ecpmPrecision;
  double ecpm;
  AdInfo(this.platformName, this.ecpmPrecision, this.ecpm);
}

class AdErrorInfo {
  int errorCode;
  String errorDescription;
  AdErrorInfo(this.errorCode, this.errorDescription);
}
