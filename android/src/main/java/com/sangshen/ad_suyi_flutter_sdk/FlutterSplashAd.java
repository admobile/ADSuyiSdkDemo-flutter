package com.sangshen.ad_suyi_flutter_sdk;

import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.flutter.plugin.platform.PlatformView;

public class FlutterSplashAd extends FlutterAd implements FlutterDestroyableAd {
    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;
    /**
     * 背景图片
     */
    private final String imgName;
    /**
     * Logo图片
     */
    private final String imgLogoName;
    /**
     * 是否允许重复读取权限
     */
    private final boolean isRepeatApplyPermission;
    /**
     * 是否允许动态申请权限
     */
    private final boolean isApplyPermission;

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;

        private String imgName;

        private String imgLogoName;

        private boolean isRepeatApplyPermission;

        private boolean isApplyPermission;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        public Builder setImgName(@NonNull String imgName) {
            this.imgName = imgName;
            return this;
        }

        public Builder setImgLogoName(@NonNull String imgLogoName) {
            this.imgLogoName = imgLogoName;
            return this;
        }

        public Builder setRepeatApplyPermission(@NonNull boolean isRepeatApplyPermission) {
            this.isRepeatApplyPermission = isRepeatApplyPermission;
            return this;
        }

        public Builder setApplyPermission(@NonNull boolean isApplyPermission) {
            this.isApplyPermission = isApplyPermission;
            return this;
        }

        FlutterSplashAd build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterSplashAd bannerAd = new FlutterSplashAd(manager, adPosId, imgName, imgLogoName, isRepeatApplyPermission, isApplyPermission);
            return bannerAd;
        }
    }

    private FlutterSplashAd(
            @NonNull AdInstanceManager manager, @NonNull String adPosId, String imgName, String imgLogoName, boolean isRepeatApplyPermission, boolean isApplyPermission) {
        this.manager = manager;
        this.adPosId = adPosId;
        this.imgName = imgName;
        this.imgLogoName = imgLogoName;
        this.isRepeatApplyPermission = isRepeatApplyPermission;
        this.isApplyPermission = isApplyPermission;
    }

    @Override
    void load() {
        SplashAdActivity.startActivity(manager.activity, manager, manager.adIdFor(this), adPosId, imgName, imgLogoName, isRepeatApplyPermission, isApplyPermission);
    }

    @Override
    public void release() {

    }
}
