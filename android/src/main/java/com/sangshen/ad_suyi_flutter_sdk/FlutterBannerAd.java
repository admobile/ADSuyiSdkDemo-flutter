package com.sangshen.ad_suyi_flutter_sdk;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.admobiletop.adsuyi.ad.ADSuyiBannerAd;
import cn.admobiletop.adsuyi.ad.data.ADSuyiAdInfo;
import cn.admobiletop.adsuyi.ad.error.ADSuyiError;
import cn.admobiletop.adsuyi.ad.listener.ADSuyiBannerAdListener;
import cn.admobiletop.adsuyi.util.ADSuyiDisplayUtil;
import io.flutter.plugin.platform.PlatformView;

public class FlutterBannerAd extends FlutterAd implements PlatformView, FlutterDestroyableAd {
    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;
    /**
     * 场景id可选
     */
    @NonNull
    private final String sceneId;
    /**
     * 广告宽
     */
    @NonNull
    private final Double adWidth;
    /**
     * 广告高
     */
    @NonNull
    private final Double adHeight;
    /**
     * 广告容器
     */
    private FrameLayout flContainer;
    /**
     * 广告对象
     */
    private ADSuyiBannerAd adSuyiBannerAd;

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;
        @Nullable
        private String sceneId;
        @Nullable
        private Double adWidth;
        @Nullable
        private Double adHeight;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        public Builder setAdWidth(@NonNull Double adWidth) {
            this.adWidth = adWidth;
            return this;
        }

        public Builder setAdHeight(@NonNull Double adHeight) {
            this.adHeight = adHeight;
            return this;
        }

        public Builder setSceneId(@NonNull String sceneId) {
            this.sceneId = sceneId;
            return this;
        }

        FlutterBannerAd build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterBannerAd bannerAd = new FlutterBannerAd(manager, adPosId, sceneId, adWidth, adHeight);
            return bannerAd;
        }
    }

    private FlutterBannerAd(
            @NonNull AdInstanceManager manager, @NonNull String adPosId, @NonNull String sceneId, @NonNull Double adWidth, @NonNull Double adHeight) {
        this.manager = manager;
        this.adPosId = adPosId;
        this.sceneId = sceneId;
        this.adWidth = adWidth;
        this.adHeight = adHeight;

        flContainer = new FrameLayout(manager.activity);
        flContainer.setLayoutParams(new ViewGroup.LayoutParams(
                ADSuyiDisplayUtil.dp2px(new Double(this.adWidth).intValue()),
                ADSuyiDisplayUtil.dp2px(new Double(this.adHeight).intValue())
            )
        );

    }

    @Override
    void load() {
        adSuyiBannerAd = new ADSuyiBannerAd(manager.activity, flContainer);
        // 设置自刷新时间间隔，0为不自动刷新（部分平台无效，如百度），其他取值范围为[30,120]，单位秒
        adSuyiBannerAd.setAutoRefreshInterval(ADSuyiDemoConstant.BANNER_AD_AUTO_REFRESH_INTERVAL);
        // 设置仅支持的广告平台，设置了这个值，获取广告时只会去获取该平台的广告，null或空字符串为不限制，默认为null，方便调试使用，上线时建议不设置
        adSuyiBannerAd.setOnlySupportPlatform(ADSuyiDemoConstant.BANNER_AD_ONLY_SUPPORT_PLATFORM);
        // 设置Banner广告监听
        adSuyiBannerAd.setListener(new ADSuyiBannerAdListener() {
            @Override
            public void onAdReceive(ADSuyiAdInfo adSuyiAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdReceive...");
                manager.onAdReceive(
                    FlutterBannerAd.this,
                        adSuyiAdInfo.getPlatform(),
                        adSuyiAdInfo.getEcpmPrecision(),
                        adSuyiAdInfo.getECPM()
                );
            }

            @Override
            public void onAdExpose(ADSuyiAdInfo adSuyiAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdExpose...");
                manager.onAdExpose(
                        FlutterBannerAd.this,
                        adSuyiAdInfo.getPlatform(),
                        adSuyiAdInfo.getEcpmPrecision(),
                        adSuyiAdInfo.getECPM()
                );
            }

            @Override
            public void onAdClick(ADSuyiAdInfo adSuyiAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdClick...");
                manager.onAdClick(
                        FlutterBannerAd.this,
                        adSuyiAdInfo.getPlatform(),
                        adSuyiAdInfo.getEcpmPrecision(),
                        adSuyiAdInfo.getECPM()
                );
            }

            @Override
            public void onAdClose(ADSuyiAdInfo adSuyiAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdClose...");
                manager.onAdClose(
                        FlutterBannerAd.this,
                        adSuyiAdInfo.getPlatform(),
                        adSuyiAdInfo.getEcpmPrecision(),
                        adSuyiAdInfo.getECPM()
                );
            }

            @Override
            public void onAdFailed(ADSuyiError adSuyiError) {
                if (adSuyiError != null) {
                    String failedJson = adSuyiError.toString();
                    Log.d(ADSuyiDemoConstant.TAG, "onAdFailed..." + failedJson);
                    manager.onAdFailed(FlutterBannerAd.this, adSuyiError);
                }
            }
        });
        // banner广告场景id（场景id非必选字段，如果需要可到开发者后台创建）
        adSuyiBannerAd.setSceneId(sceneId);
        // 加载Banner广告，参数为广告位ID，同一个ADSuyiBannerAd只有一次loadAd有效
        adSuyiBannerAd.loadAd(adPosId);
    }

    @Override
    public void release() {
        if (adSuyiBannerAd != null) {
            adSuyiBannerAd.release();
            adSuyiBannerAd = null;
        }
    }

    @Override
    public View getView() {
        return flContainer;
    }

    @Override
    public void dispose() {

    }
}
