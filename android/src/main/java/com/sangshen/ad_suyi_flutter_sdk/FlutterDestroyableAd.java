package com.sangshen.ad_suyi_flutter_sdk;

public interface FlutterDestroyableAd {
    void release();
}
