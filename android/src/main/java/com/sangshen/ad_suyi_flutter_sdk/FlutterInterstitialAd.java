package com.sangshen.ad_suyi_flutter_sdk;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.admobiletop.adsuyi.ad.ADSuyiInterstitialAd;
import cn.admobiletop.adsuyi.ad.data.ADSuyiInterstitialAdInfo;
import cn.admobiletop.adsuyi.ad.error.ADSuyiError;
import cn.admobiletop.adsuyi.ad.listener.ADSuyiInterstitialAdListener;
import cn.admobiletop.adsuyi.util.ADSuyiAdUtil;
import cn.admobiletop.adsuyi.util.ADSuyiToastUtil;

public class FlutterInterstitialAd extends FlutterAd.FlutterOverlayAd implements FlutterDestroyableAd {

    @NonNull
    private final AdInstanceManager manager;
    /**
     * 广告位id
     */
    @NonNull
    private final String adPosId;
    /**
     * 场景id可选
     */
    @NonNull
    private final String sceneId;
    /**
     * 广告对象
     */
    private ADSuyiInterstitialAd adSuyiInterstitialAd;
    /**
     * 插屏广告对象
     */
    private ADSuyiInterstitialAdInfo _interstitialAdInfo;

    @Override
    void show() {
        if (_interstitialAdInfo != null) {
            // TODO 插屏广告对象一次成功拉取的广告数据只允许展示一次
            ADSuyiAdUtil.showInterstitialAdConvenient(manager.activity, _interstitialAdInfo);
        }
    }

    static class Builder {
        @Nullable
        private AdInstanceManager manager;
        @Nullable
        private String adPosId;
        @Nullable
        private String sceneId;

        public Builder setManager(@NonNull AdInstanceManager manager) {
            this.manager = manager;
            return this;
        }

        public Builder setAdPosId(@NonNull String adPosId) {
            this.adPosId = adPosId;
            return this;
        }

        public Builder setSceneId(@NonNull String sceneId) {
            this.sceneId = sceneId;
            return this;
        }

        FlutterInterstitialAd build() {
            if (manager == null) {
                throw new IllegalStateException("AdInstanceManager cannot not be null.");
            } else if (adPosId == null) {
                throw new IllegalStateException("adPosId cannot not be null.");
            }

            final FlutterInterstitialAd interstitialAd = new FlutterInterstitialAd(manager, adPosId, sceneId);
            return interstitialAd;
        }
    }

    private FlutterInterstitialAd(
            @NonNull AdInstanceManager manager, @NonNull String adPosId, @NonNull String sceneId) {
        this.manager = manager;
        this.adPosId = adPosId;
        this.sceneId = sceneId;
    }

    @Override
    void load() {
        adSuyiInterstitialAd = new ADSuyiInterstitialAd(manager.activity);
        // 设置仅支持的广告平台，设置了这个值，获取广告时只会去获取该平台的广告，null或空字符串为不限制，默认为null，方便调试使用，上线时建议不设置
        adSuyiInterstitialAd.setOnlySupportPlatform(ADSuyiDemoConstant.INTERSTITIAL_AD_ONLY_SUPPORT_PLATFORM);
        // 设置插屏广告监听
        adSuyiInterstitialAd.setListener(new ADSuyiInterstitialAdListener() {
            @Override
            public void onAdReady(ADSuyiInterstitialAdInfo interstitialAdInfo) {
                // 建议在该回调之后展示广告
                Log.d(ADSuyiDemoConstant.TAG, "onAdReady...");
            }

            @Override
            public void onAdReceive(ADSuyiInterstitialAdInfo interstitialAdInfo) {
                _interstitialAdInfo = interstitialAdInfo;
                Log.d(ADSuyiDemoConstant.TAG, "onAdReceive...");
                manager.onAdReceive(
                    FlutterInterstitialAd.this,
                        interstitialAdInfo.getPlatform(),
                        interstitialAdInfo.getEcpmPrecision(),
                        interstitialAdInfo.getECPM()
                );
            }

            @Override
            public void onAdExpose(ADSuyiInterstitialAdInfo interstitialAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdExpose...");
                manager.onAdExpose(
                        FlutterInterstitialAd.this,
                        interstitialAdInfo.getPlatform(),
                        interstitialAdInfo.getEcpmPrecision(),
                        interstitialAdInfo.getECPM()
                );
            }

            @Override
            public void onAdClick(ADSuyiInterstitialAdInfo interstitialAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdClick...");
                manager.onAdClick(
                        FlutterInterstitialAd.this,
                        interstitialAdInfo.getPlatform(),
                        interstitialAdInfo.getEcpmPrecision(),
                        interstitialAdInfo.getECPM()
                );
            }

            @Override
            public void onAdClose(ADSuyiInterstitialAdInfo interstitialAdInfo) {
                Log.d(ADSuyiDemoConstant.TAG, "onAdClose...");
                manager.onAdClose(
                        FlutterInterstitialAd.this,
                        interstitialAdInfo.getPlatform(),
                        interstitialAdInfo.getEcpmPrecision(),
                        interstitialAdInfo.getECPM()
                );
            }

            @Override
            public void onAdFailed(ADSuyiError adSuyiError) {
                if (adSuyiError != null) {
                    String failedJson = adSuyiError.toString();
                    Log.d(ADSuyiDemoConstant.TAG, "onAdFailed..." + failedJson);
                    manager.onAdFailed(FlutterInterstitialAd.this, adSuyiError);
                }
            }
        });

        // 设置场景id
        adSuyiInterstitialAd.setSceneId(sceneId);
        // 加载插屏广告，参数为广告位ID
        adSuyiInterstitialAd.loadAd(adPosId);
    }

    @Override
    public void release() {
        if (adSuyiInterstitialAd != null) {
            adSuyiInterstitialAd.release();
        }
    }
}
